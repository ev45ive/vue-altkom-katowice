import _Vue, { PluginObject, ComponentOptions } from "vue"

// https://www.mistergoodcat.com/post/vuejs-plugins-with-typescript


// export const Placki: PluginObject<any> = {
//     install(vue: typeof _Vue, options: any) {
//         vue.prototype.$placki = 123
//     }
// }

// export type PluginFunction<T> = (Vue: typeof _Vue, options?: T) => void;
export function Placki(Vue: typeof _Vue, options?: any): void {
    Vue.prototype.$placki = {
        value:123
    };
}