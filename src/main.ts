import Vue, {  } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import { authService } from './services/AuthService';
import { musicSearch } from './services/MusicSearchService';

// Plugin Resource
import VueResource from 'vue-resource'
import { Placki } from './Placki.plugin';
import './components'
import Component from 'vue-class-component';

Vue.config.productionTip = false;

authService.getToken()
musicSearch


Vue.use(VueResource, {})
Vue.use(Placki, {})



// Register the router hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
])


const app = new Vue({
  router,
  store,
  http: {
    root: '/root',
    headers: {
      Authorization: 'Basic YXBpOnBhc3N3b3Jk'
    }
  },
  render: (h) => h(App),
}).$mount('#app');


(window as any).app = app;