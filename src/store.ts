import Vue from 'vue';
import Vuex from 'vuex';
import { Counterstate, counter } from './modules/counter';
import PlaylistsModule from './modules/playlists';
import MusicSearchModule from './modules/MusicSearchModule';

Vue.use(Vuex);

export type AppState = {
  counter: Counterstate
}

export default new Vuex.Store<AppState>({
  modules: {
    counter: counter,
    playlists: PlaylistsModule,
    musicSearch: MusicSearchModule
  }
});





/*
const magic = ({
  commit,
  state: { counter },
  dispatch
}) => ({
  commit,
  state: { placki: counter },
  dispatch,
  placki(){

  }
}) */
